class Searcher
  def initialize(params)
    @params = default_params.merge(params)
  end

  def self.for_params(params = {})
    new(params)
  end

  def results(page: 1, per_page: 50)
    page ||= 1

    pipeline = []

    if ! @params['query'].empty? && @params['location'].empty?
      pipeline << {
        "$match" => {
          t: /#{@params['query']}/i
        }
      }
    end

    unless @params['location'].empty?
      geoNear = {
        "near"               => coordinates,
        "distanceField"      => "distance",
        "distanceMultiplier" => 3959,
        "num"                => 500,
      # "maxDistance"        => 1.fdiv(3959),
        "spherical"          => true,
      }

      unless @params['query'].empty?
        geoNear["query"] = { 
          t: /#{@params['query']}/i
        }
      end

      pipeline << {
        "$geoNear" => geoNear
      }
    else
      pipeline << {
        "$sort" => {
          p: -1
        }
      }
    end

    count = aggregate(pipeline).count

    pipeline << { "$skip" => ((page.to_i - 1) * per_page) }
    pipeline << { "$limit" => per_page }

    places_hash = aggregate(pipeline)

    places = places_hash.map { |attrs| Offer.new(attrs) { |o| o.new_record = false } }

    unless @params['location'].empty?
      # Do we need this? Left for experimetns...
      # It needs refactoring, but this is not good time to distilling
      # abstraction. More ideas will come when applying filtering only
      # published offers, or within specified category.
      # places = Searcher.inject_distances_and_sort(places, places_hash)
    end

    places.instance_eval <<-EVAL
      def current_page
        #{page}
      end

      def total_pages
        #{(count.to_f / per_page).ceil}
      end

      def limit_value
        #{per_page}
      end
    EVAL

    places
  end

  private

  def default_params
    { 'query' => '', 'location' => '' }
  end

  def aggregate(pipeline)
    Offer.collection.aggregate(pipeline)
  end

  # Left for experiments...
  class << self
#   def inject_distances_and_sort(places, places_hash)
#     places = places.each do |place|
#       place.distance = places_hash.find(->{ { "distance" => 0 } }) { |p| p["_id"] == place.id }["distance"]
#     end

#     places.sort! { |a, b| a.distance <=> b.distance }
#   end
  end

  def coordinates
    location.coordinates
  end

  def location
    @location ||= Location::Lookup.for(@params['location']).location
  end
end
