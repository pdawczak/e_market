class User
  include Mongoid::Document
  include Mongoid::Timestamps::Created::Short
  include Mongoid::Timestamps::Updated::Short

  field :p, as: :provider,  type: String
  field :u, as: :uid,       type: String
  field :n, as: :name,      type: String
  field :i, as: :image_url, type: String

  field :t, as: :oauth_token,      type: String
  field :e, as: :oauth_expires_at, type: Time

  def logged_in?
    true
  end

  class << self
    def from_omniauth(auth)
      where(provider: auth.provider, uid: auth.uid).first_or_initialize do |user|
        user.provider = auth.provider
        user.uid = auth.uid
        user.name = auth.info.name
        user.image_url = auth.info.image
        user.oauth_token = auth.credentials.token
        user.oauth_expires_at = Time.at(auth.credentials.expires_at)
        user.save!
      end
    end

    def fetch(id)
      begin
        find(id)
      rescue Mongoid::Errors::DocumentNotFound, Mongoid::Errors::InvalidFind
        Anonymous.new
      end
    end
  end
  
  class Anonymous
    def name
      'Anonymous'
    end

    def logged_in?
      false
    end
  end
end
