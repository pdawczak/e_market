class Location
  include Mongoid::Document
  include Mongoid::Timestamps::Created::Short
  include Mongoid::Timestamps::Updated::Short
  include Geocoder::Model::Mongoid

  geocoded_by :location
  after_validation :geocode, if: ->(obj) { obj.coordinates.nil? }

  field :l, as: :location,    type: String
  field :c, as: :canonical,   type: String
  field :p, as: :coordinates, type: Array

  index({ c: '2d' }, { min: -200, max: 200 })

  DICTIONARY = %w{new ely}

  def location=(location)
    self[:canonical] = Canoniser.canonise(location)

    process_part = -> (string) {
      return string if string.size < 4 && DICTIONARY.none? { |dict| dict == string.downcase }
      string.downcase.titleize
    }

    super location.split(' ').map(&process_part).join(' ')
  end

  class Canoniser
    def initialize(string)
      @string = string
    end

    class << self
      def canonise(string)
        new(string).canonise
      end
    end

    def canonise
      @canonised ||= process_string
    end

    private

    def process_string
      @string.delete(',').upcase
    end
  end

  class Lookup
    def initialize(location_string)
      @location_string = location_string
    end

    def self.for(location_string)
      new(location_string)
    end

    def location
      Location.find_or_create_by(canonical: canonise) do |location|
        location.location = @location_string
      end
    end

    private

    def canonise
      Canoniser.canonise(@location_string)
    end
  end
end
