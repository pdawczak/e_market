class Offer
  include Mongoid::Document
  include Mongoid::Timestamps::Created::Short
  include Mongoid::Timestamps::Updated::Short

  attr_accessor :distance

  field :t, as: :title,        type: String
  field :p, as: :published_at, type: Time

  field :l, as: :location,    type: String
  field :c, as: :coordinates, type: Array

  index({ c: '2d' }, { min: -200, max: 200 })
end
