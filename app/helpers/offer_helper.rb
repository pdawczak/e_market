module OfferHelper
  def distance_for(offer)
    return '< 5 mi' if offer.distance < 5
    offer.distance.to_f.round(2).to_s + ' mi'
  end

  def format_distance_for(offer)
    content_tag(:em, distance_for(offer), { class: 'distance' }) unless offer.distance.nil?
  end
end
