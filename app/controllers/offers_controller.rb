class OffersController < ApplicationController
  def index
    @offers = Offer.order_by(:published_at.desc)
      .page(params[:page])
  end

  def search
    @offers = Searcher.for_params(params).results(page: params[:page])
  end
end
