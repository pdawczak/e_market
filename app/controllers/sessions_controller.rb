class SessionsController < ApplicationController
  def create
    user = User.from_omniauth(env["omniauth.auth"])
    session[:user_id] = user.id.to_s
    flash[:success] = "Welcome #{user.name}. You have signed in successfully!"
    redirect_to previous_page
  end

  def failure
    flash[:error] = 'Oh snap... Something went wrong. Please try again.'
    redirect_to previous_page
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url
  end

  private

  def previous_page
    request.env['omniauth.origin'] || root_url
  end
end
