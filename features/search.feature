Feature: Search
  In order to find interesint offers
  As a visitor
  I want to be able to search through offers

  Scenario: Simple search by keyword
    Given there are offers like:
      | title                 |
      | Dyson DC              |
      | Nokia lumia 625       |
      | Nintendo Wii          |
      | Sample vacuum cleaner |
      | Beautiful shirt       |
      | Candy-Crush T-shirt   |
      | Shirt-hook            |
      | Bermud shorts         |
      | A hat                 |
      | Metallica-band tshirt |
    When I am on homepage
     And perform searching by keyword "shirt"
    Then I should see offers: Beautiful shirt, Candy-Crush T-shirt, Shirt-hook, Metallica-band tshirt
     But I should not see offers: Dyson DC, Nokia lumia 625, Nintendo Wii, Sample vacuum cleaner, Bermud shorts, A hat
  
  Scenario: Searching near location
    Given there are offers like:
      | title     | location | coordinates           |
      | Dyson DC  | CB6 3WL  | 52.4069251, 0.2499793 |
      | Hat       | CB6 1AE  | 52.4021065, 0.2622551 |
      | Desk      | CB6 3BZ  | 52.3970572, 0.2456053 |
     And there are following locations defined:
      | location | canonical | coordinates          |
      | Ely      | ELY       | 52.3929783, 0.275411 |
    When I am on homepage
     And I perform searching near location "Ely"
    Then offer "Hat" should be before "Dyson DC"
     And offer "Dyson DC" should be before "Desk"
