Feature: Paginating offer lists
  In order to easier check interesting offers
  As a visitor
  I want to be able to check the list of offers as paginated results

  Background:
    Given there are at least 55 offers created chronologically
     When I am on homepage

  Scenario: Render paginator
     Then I should see a pagination panel

  Scenario: Homepage results - first page
     Then I should see offer "Offer no 55"
      But I should not see offer "Offer no 01"
  
  Scenario: Homepage results - navigate to second page
    When I go to 2 page
    Then I should see offer "Offer no 01"
     But I should not see offer "Offer no 55"

  Scenario: Search results - first page
    When I perform searching by keyword "Offer"
    Then I should see offer "Offer no 55"
     But I should not see offer "Offer no 01"

  Scenario: Search results - navigate to second page
    When I perform searching by keyword "offer"
     And I go to 2 page
    Then I should see filtering form persists filtering by keyword "offer"
     And I should not see offer "Offer no 55"
     But I should see offer "Offer no 01"
