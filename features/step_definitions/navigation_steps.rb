Given(/^I am on homepage$/) do
  visit root_path
end

Then(/^show the page$/) do
  save_and_open_page
end

Then(/^I should see a pagination panel$/) do
  expect(page).to have_selector(".pagination")
end

When(/^I go to (\d+) page$/) do |page_number|
  within(".pagination") do
    click_on page_number
  end
end
