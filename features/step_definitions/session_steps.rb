Then(/^I should see that I'm not logged in$/) do
  expect(page).to have_selector("#login-btn")
end

When(/^I log in with button$/) do
  click_on("login-btn")
end

Then(/^I should see that I am logged in$/) do
  expect(page).to have_content("Welcome")
  expect(page).to have_selector("#logout-btn")
end

When(/^I log out$/) do
  click_on("logout-btn")
end
