Given(/^there are following locations defined:$/) do |locations_table|
  locations_table.hashes.each do |attrs|
    attrs['coordinates'] = attrs['coordinates'].split(', ').map(&:to_f) unless attrs['coordinates'].nil?
    Location.create(attributes_for(:location).merge(attrs))
  end
end
