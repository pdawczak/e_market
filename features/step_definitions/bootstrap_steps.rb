Given(/^I am on static homepage$/) do
  visit '/static/index'
end

Then(/^I want to see "(.*?)"$/) do |expected_text|
  expect(page).to have_content expected_text
end
