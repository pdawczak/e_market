Given(/^there are offers like:$/) do |offers_table|
  offers_table.hashes.each do |attrs|
    attrs['coordinates'] = attrs['coordinates'].split(', ').map(&:to_f) if attrs.has_key?('coordinates')
    Offer.create(attributes_for(:offer).merge(attrs))
  end
end

Then(/^I should( not)? see offer "(.*?)"$/) do |not_see, offer_title|
  method = not_see ? :not_to : :to
  expect(page.find('.offers')).send(method, have_content(offer_title))
end

Then(/^I should( not)? see offers: (.*?)$/) do |not_see, offers_string|
  not_see_string = not_see ? ' not' : ''
  offers_string.split(', ').map do |title|
    step "I should#{not_see_string} see offer \"#{title}\""
  end
end

Then(/^offer "(.*?)" should be before "(.*?)"$/) do |first_offer_title, second_offer_title|
  first_index  = page.body.index first_offer_title
  second_index = page.body.index second_offer_title

  expect(first_index).to be < second_index
end

When(/^(I )?perform searching by keyword "(.*?)"$/) do |option, keyword|
  within("#search-form") do
    fill_in "query", with: keyword
    click_button "Search"
  end
end

When(/^I perform searching near location "(.*?)"$/) do |location|
  within("#search-form") do
    fill_in "location", with: location
    click_button "Search"
  end
end

Given(/^there are at least (\d+) offers created chronologically$/) do |count|
  count_length = count.to_s.length

  count.times do |num|
    number_of    = count - num
    offer_number = (num + 1).to_s.rjust(count_length, "0")

    Offer.create(attributes_for(:offer, title: "Offer no #{offer_number}", 
                                 published_at: number_of.days.ago))
  end
end

Then(/^I should see filtering form persists filtering by keyword "(.*?)"$/) do |expected_text|
  within("#search-form") do
    expect(page).to have_field("query", with: expected_text)
  end
end

Transform(/^(-?\d+)$/) do |number|
  number.to_i
end
