Feature: Sessions
  In order to be able to see offer details
  As a visitor
  I want to be able to log in using Facebook

  Scenario: Status of not logged in
    Given I am on homepage
     Then I should see that I'm not logged in

  Scenario: Logging in
    Given I am on homepage
     When I log in with button
     Then I should see that I am logged in

  Scenario: Logging out
    Given I am on homepage
     When I log in with button
      And I log out
     Then I should see that I'm not logged in
