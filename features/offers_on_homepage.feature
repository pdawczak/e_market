Feature: Offers on homepage
  In order to check offers
  As a guest
  I want to see the newest offers

  Scenario: Newes offers
    Given there are offers like:
      | title           | published_at |
      | Dyson DC        | 2014-06-09   |
      | Nokia lumia 625 | 2014-03-12   |
      | Nintendo Wii    | 2014-09-23   |
     When I am on homepage
     Then I should see offers: Nintendo Wii, Dyson DC, Nokia lumia 625
     And offer "Nintendo Wii" should be before "Dyson DC"
     And offer "Dyson DC" should be before "Nokia lumia 625"
