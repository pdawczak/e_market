offers = [
  {
    title:        'Metallica-band tshirt',
    published_at: Date.parse('2014-03-09'),
    location:     'CB6 1AE',
    coordinates:  [0.2622551, 52.4021065]
  },
  {
    title:        'Nice shirt',
    published_at: Date.parse('2014-08-11'),
    location:     'CB6 3NW',
    coordinates:  [0.2226021, 52.3797803]
  },
  {
    title:        'T-Shirt hook',
    published_at: Date.parse('2013-12-15'),
    location:     'CB6 2HY',
    coordinates:  [0.2057893, 52.39181]
  },
  {
    title:        'Desk',
    published_at: Date.parse('2014-02-03'),
    location:     'Catteris',
    coordinates:  [0.0891608, 52.4383006]
  },
  {
    title:        'Book',
    published_at: Date.parse('2014-10-11'),
    location:     'Coveney',
    coordinates:  [0.188006, 52.4164154]
  },
  {
    title:        'IPhone',
    published_at: Date.parse('2014-05-23'),
    location:     'Witcham',
    coordinates:  [0.149441, 52.3984055]
  }
]

# offers.each { |attrs| Offer.create(attrs) }

100_000.times do |num|
  attrs = {
    title: "Offer no #{num + 1}",
    published_at: (100_000 - num).hours.ago,
    location: 'Witcham',
    coordinates: [0.149441, 52.3984055]
  }

  Offer.create(attrs)
end
