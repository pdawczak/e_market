namespace :em do
  desc "Performing import to database"
  task import: :environment do
    puts "=" * 100
    puts "Importing"
    puts "=" * 100
  end

end
