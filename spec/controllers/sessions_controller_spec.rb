require 'rails_helper'

RSpec.describe SessionsController, :type => :controller do

  describe "GET create" do
    before do
      OmniAuth.config.test_mode = true
      OmniAuth.config.add_mock(:facebook, {
        uid:  '12345',
        user_info: {
          name:  'Sampler the Test',
          image: '/a-sample/image.png'
        },
        credentials: {
          token:      'zxcasdqwe',
          expires_at: 12345678
        }
      })

      allow(@controller).to receive(:env).and_return({ 
        'omniauth.auth' => OmniAuth.config.mock_auth[:facebook]
      })
    end

    after do
      OmniAuth.config.mock_auth[:facebook] = nil
    end

    context "visitor has not logged in before" do
      it "creates new user" do
        expect {
          get :create, provider: 'facebook'
        }.to change(User, :count).by 1
      end
    end

    context "visitor has logged in before" do
      before do
        create(:user, uid: '12345')
      end

      it "doesn't create new User entry" do
        expect {
          get :create, provider: 'facebook'
        }.not_to change(User, :count)
      end
    end
  end

  describe "GET failure" do
    before do
      OmniAuth.config.test_mode = true
      OmniAuth.config.mock_auth[:facebook] = :invalid_credentials

      allow(@controller).to receive(:env).and_return({ 
        'omniauth.auth' => OmniAuth.config.mock_auth[:facebook]
      })
    end

    after do
      OmniAuth.config.mock_auth[:facebook] = nil
    end

    it "redirects from current page" do
      get :failure
      expect(response).to have_http_status(:redirect)
    end

    it "populates notification" do
      get :failure
      expect(flash[:error]).to be_present
    end
  end

  describe "GET destroy" do
    it "logs use rout" do
      session[:user_id] = { '$oid' => '12345' }
      get :destroy
      expect(session[:user_id]).to be_nil
    end

    it "redirects to homepage" do
      get :destroy
      expect(response).to redirect_to root_path
    end
  end

end
