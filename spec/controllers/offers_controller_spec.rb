require 'rails_helper'

RSpec.describe OffersController, :type => :controller do

  describe "GET index" do
    it "fetches offers from database ordered by publisched_at DESC" do
      offer_1 = Offer.create(title: "Sample Offer", published_at: Date.parse('2014-05-01'))
      offer_2 = Offer.create(title: "Another Offer", published_at: Date.parse('2014-08-22'))
      offer_3 = Offer.create(title: "One more Offer", published_at: Date.parse('2014-02-12'))
      get :index
      expect(assigns(:offers)).to eq [offer_2, offer_1, offer_3]
    end
  end

end
