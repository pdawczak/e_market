require 'rails_helper'

RSpec.describe OfferHelper, :type => :helper do
  def offer_with_distance(distance)
    OpenStruct.new(distance: distance)
  end

  describe "#distance_for" do
    context "when disntance less than 5 miles" do
      it "render proper information" do
        expect(helper.distance_for(offer_with_distance(1))).to eq "< 5 mi"
      end
    end

    context "when distance more than 5 miles" do
      it "rendes two-decimal digit" do
        expect(helper.distance_for(offer_with_distance(6.789123))).to eq '6.79 mi'
      end
    end
  end
  
  describe "#format_distance" do
    context "for distance less than 5 miles" do
      it "nicely formats the information" do
        expect(helper.format_distance_for(offer_with_distance(2))).to eq '<em class="distance">&lt; 5 mi</em>'
      end
    end

    context "for distance more than 5 miles" do
      it "formats distance" do
        expect(helper.format_distance_for(offer_with_distance(8.7654))).to eq '<em class="distance">8.77 mi</em>'
      end
    end

    context "in case no distance is specified for offer" do
      it "doesn't render anything" do
        offer = Offer.new
        expect(helper.format_distance_for(offer)).to eq nil
      end
    end
  end
end
