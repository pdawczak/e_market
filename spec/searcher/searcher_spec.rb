require 'rails_helper'

RSpec.describe Searcher do
  it 'Initialises searcher for params' do
    searcher = Searcher.for_params({})
    expect(searcher).to be_kind_of Searcher
  end

  describe "#results" do
    before do
      create(:ely)

      @metallica_tshirt = create(:offer, title: 'Metallica-band T-Shirt',
                                  published_at: Date.parse('2014-03-26'),
                                   coordinates: [52.4069251, 0.2499793])

      @iphone = create(:offer, title: 'IPhone',
                        published_at: Date.parse('2014-07-03'),
                         coordinates: [52.4021065, 0.2622551])

      @hook = create(:offer, title: 'Hook for shirts',
                      published_at: Date.parse('2014-05-14'),
                       coordinates: [52.3970572, 0.2456053])
    end

    context "when query specified" do
      let(:searcher) { Searcher.for_params 'query' => 'shirt' }

      it "returns offers containing query in title" do
        expect(searcher.results).to include @metallica_tshirt
        expect(searcher.results).to include @hook
        expect(searcher.results).not_to include @iphone
      end

      it "returns offers sorted by publised_at" do
        expect(searcher.results).to eq [@hook, @metallica_tshirt]
      end
    end

    context "when location specified" do
      let(:searcher) { Searcher.for_params 'location' => 'Ely' }

      it "returns offers sorted by distance" do
        expect(searcher.results).to eq [@iphone, @metallica_tshirt, @hook]
      end
    end

    context "when both query and location specified" do
      let(:searcher) { Searcher.for_params 'query' => 'shirt', 'location' => 'Ely' }

      it "returns offers that title match specified criteria, sorted by distance" do
        expect(searcher.results).to eq [@metallica_tshirt, @hook]
      end
    end
  end
end
