FactoryGirl.define do
  factory :location do
    location "New York, NY"
    canonical "NEW YORK NY"
    coordinates [12, 34]
  end

  factory :ely, class: Location do
    location "Ely"
    canonical "ELY"
    coordinates [52.3929783, 0.275411]
  end
end
