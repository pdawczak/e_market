FactoryGirl.define do
  factory :user do
    provider         "facebook"
    uid              "123456"
    name             "Sampler the Test"
    oauth_token      "zxcasdqwe"
    oauth_expires_at "2014-12-16 21:28:56"
  end
end
