require 'rails_helper'

RSpec.describe User, :type => :model do
  subject { build(:user) }
  let(:user) { subject }

  it { is_expected.to be_valid }
  it { is_expected.to be_logged_in }

  context "Anonymous" do
    subject { User::Anonymous.new }

    it { expect(user.name).to eq 'Anonymous' }
    it { is_expected.not_to be_logged_in }
  end

  describe "#fetch" do
    context "when fetching user with valid id" do
      let(:user) { create(:user) }

      it "returns proper user object" do
        expect(User.fetch(user.id)).to eq user
      end
    end

    context "when using invalid id for fetching the user" do
      let(:user) { create(:user) }

      it "returns Anonymous user" do
        id = user.id.to_s + '-for-invalid-id'
        expect(User.fetch(id)).to be_kind_of User::Anonymous
      end
    end

    context "when trying to fetch a user by 'nil'" do
      it "returns Anonymous user" do
        expect(User.fetch(nil)).to be_kind_of User::Anonymous
      end
    end
  end
end
