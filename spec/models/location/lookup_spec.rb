require 'rails_helper'

RSpec.describe Location::Lookup do
  it 'is initialisable by location string' do
    lookup = Location::Lookup.for('Cambridge')
    expect(lookup).to be_kind_of Location::Lookup
  end

  describe "#location" do
    let(:lookup) { Location::Lookup.for('New York, NY') }

    context "when specified location is not found in database" do
      before do
        Geocoder.configure(:lookup => :test)

        Geocoder::Lookup::Test.add_stub(
          "New York, NY", [
            {
              'latitude'     => 40.7143528,
              'longitude'    => -74.0059731,
              'address'      => 'New York, NY, USA',
              'state'        => 'New York',
              'state_code'   => 'NY',
              'country'      => 'United States',
              'country_code' => 'US'
            }
          ]
        )
      end

      it "creates new entry" do
        expect {
          lookup.location
        }.to change { Location.count }.by 1
      end

      context "created location" do
        let(:location) { lookup.location }

        it { expect(location.canonical).to eq 'NEW YORK NY' }
        it { expect(location.location).to eq 'New York, NY' }
        it { expect(location.coordinates).to eq [-74.0059731, 40.7143528] }
      end
    end

    context "when location can be found in database" do
      before do
        create(:location, canonical: 'NEW YORK NY')
      end

      it "doesn't create new location if specified location is exactly the same" do
        expect {
          lookup.location
        }.to change { Location.count }.by 0
      end
    end
  end
end
