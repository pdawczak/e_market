require 'rails_helper'

RSpec.describe Location::Canoniser do
  describe ".canonise" do
    it "upcases the string" do
      expect(Location::Canoniser.new('SamPle').canonise).to eq 'SAMPLE'
    end

    it "additionally removes comas" do
      expect(Location::Canoniser.new('London, UK').canonise).to eq 'LONDON UK'
    end
  end

  describe "#canonise" do
    it "performs full canonisation" do
      expect(Location::Canoniser.canonise('New York, NY')).to eq 'NEW YORK NY'
    end
  end
end
