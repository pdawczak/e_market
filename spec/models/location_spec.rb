require 'rails_helper'

RSpec.describe Location, :type => :model do
  subject { build(:location) }
  it { is_expected.to be_valid }

  context "creating location" do
    before do
      Geocoder.configure(:lookup => :test)

      Geocoder::Lookup::Test.add_stub(
        "New York, NY", [
          {
            'latitude'     => 40.7143528,
            'longitude'    => -74.0059731,
            'address'      => 'New York, NY, USA',
            'state'        => 'New York',
            'state_code'   => 'NY',
            'country'      => 'United States',
            'country_code' => 'US'
          }
        ]
      )
    end

    let(:location) { Location.new(location: 'New York, NY') }

    it "populates geocoordinates" do
      location.valid?
      expect(location.coordinates).to eq [-74.0059731, 40.7143528]
    end

    it "sets 'canonical'" do
      expect(location.canonical).to eq "NEW YORK NY"
    end

    it "cleans location name" do
      location = Location.new(location: 'new york')
      expect(location.location).to eq 'New York'

      location = Location.new(location: 'nEw yOrK')
      expect(location.location).to eq 'New York'

      location = Location.new(location: 'nEw yOrK, USA')
      expect(location.location).to eq 'New York, USA'

      location = Location.new(location: 'ely')
      expect(location.location).to eq 'Ely'
    end
  end
end
